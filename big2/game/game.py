"""
Games series:
  multiple games (what the program is for), runs indefinitely until user quits the program
Game:
  a whole game where 3 diamond/previous winner starts and ends when a player has no more cards
Round:
  these goes on until 3 consecutive players pass and previous player starts a new round
"""
import random
from ..cards.card import Card, Rank, Suit, pprint
from ..cards import combo_check
from ..cards.combo_check import Combo
from collections import Counter


class StdMessageOutput:
    def __init__(self):
        pass

    def write_line(self, message_string):
        print(message_string)


class Game:
    def __init__(self, players, starting_player_index=None):
        self.table = []  # note that the table clears every round.
        self.game_log = [] # keeps record of hands played, passess, start next round
        self.deck = []
        self.players = players
        self.current_player_index = starting_player_index

        self.log = StdMessageOutput()

        self.init_game()

    def init_game(self):
        """ Set up new game data. """

        self.log.write_line('*** Welcome to Big 2! ***')
        self.log.write_line('Players:')
        for i, player in enumerate(self.players, 1):
            self.log.write_line("  ({}) {.name}".format(i, player))

        self.table = []

        self.log.write_line("Shuffling and dealing...")
        self.make_new_deck()

        # A player may not have face cards, requiring reshuffle and re-deal.
        while True:
            self.shuffle_deck()
            self.distribute_deck()

            # check if there is a player who has no face cards
            need_reshuffle = False
            for player in self.players:
                if all(c.rank < Rank.JACK for c in player.hand):
                    need_reshuffle = True
                    break
            if not need_reshuffle:
                break

    def make_new_deck(self):
        """ Generate 52-card deck in ascending order. """
        self.deck = [Card(r, s) for r in Rank for s in Suit]

    def shuffle_deck(self):
        """ Shuffle deck once. """
        random.shuffle(self.deck)

    def distribute_deck(self):
        """ Give out cards to players.

        Assumes that there are 52 cards in the deck.
        """
        for i, player in enumerate(self.players):
            player.hand = self.deck[i * 13:(i + 1) * 13]

    def next_player(self):
        """ Set the current player to the next player (index). """
        if self.current_player_index is None:
            # No current player (i.e. round hasn't started)
            # Sort players according their lowest card (e.g. 3 diamond starts)
            self.players.sort(key=lambda player: min(player.hand))
            self.current_player_index = 0
        else:
            self.current_player_index += 1
            self.current_player_index %= len(self.players)
        return self.current_player_index

    def previous_combination(self):
        """ Return the last card combination put on the table. """
        if len(self.table) == 0:
            return None
        else:
            return self.table[-1]

    def valid_combination(self, curr_cards):
        """ Check if the given cards can be played.

        The following must be satisfied:
          - same number of cards as the previous play
          - the selected combination is "greater" than the previous combination

        Assumes that the previous combination is valid
        """
        prev_cards = self.previous_combination()
        # Empty table (no previous card combination to compare with
        if prev_cards is None:
            curr_type = combo_check.get_combo_type(curr_cards)
            return curr_type is not None
        if len(prev_cards) != len(curr_cards):
            return False
        prev_type = combo_check.get_combo_type(prev_cards)
        curr_type = combo_check.get_combo_type(curr_cards)
        if curr_type is None:
            return False
        if prev_type != curr_type:
            return prev_type < curr_type
        if curr_type in (
                Combo.SINGLE,
                Combo.PAIR,
                Combo.TRIPLE,
                Combo.QUADRUPLE,
                Combo.STRAIGHT,
                Combo.STRAIGHT_FLUSH):
            return max(prev_cards) < max(curr_cards)
        if curr_type == Combo.FLUSH:
            ps = prev_cards[0].suit
            ss = curr_cards[0].suit
            if ps != ss:
                return ps < ss
            else:
                return max(prev_cards) < max(curr_cards)
        if curr_type == [Combo.FULL_HOUSE, Combo.FOUR_OF_A_KIND]:
            rank_count_p = Counter(c.rank for c in prev_cards)
            rank_count_c = Counter(c.rank for c in curr_cards)
            pr = rank_count_c.most_common(1)[0]
            cr = rank_count_p.most_common(1)[0]
            return pr < cr

    def combination_put_down(self, cards, player):
        assert player in self.players
        self.table.append(cards)
        for card in cards:
            player.hand.remove(card)
        self.log.write_line('{} played {}'.format(
            player.name, [pprint(card) for card in cards]))

    def run_game_loop(self):
        """ Executes a single game loop
        Notes:
        - assumes everything was initialized properly in the constructor
        - a player's choice is either a pass (denoted by an empty list)
          or a deal (a nonempty list of cards). May change this later to
          an enum (e.g. class PlayerChoice(enum.Enum): PASS = 0; DEAL = 1;
        """
        pass_count = 0
        self.log.write_line('Game Begins Now!')
        while True:
            i = self.next_player()
            player = self.players[i]

            # loop until the player either passes or deals a valid combination
            choice = None
            while True:
                choice = player.make_choice()
                choose_pass = len(choice) == 0
                # pass
                if choose_pass:
                    pass_count += 1
                    if pass_count == 3:
                        self.table.clear()
                        pass_count = 0
                    break
                # deal
                if self.valid_combination(choice):
                    self.combination_put_down(choice, player)
                    pass_count = 0
                    break
                # else, keep repeating by default until the player makes a valid choice
                
            # keep track of players
            self.game_log.append((player, choice))
            
            # check if the current player won
            if len(player.hand) == 0:
                return self.players[i]
