import datetime
from enum import Enum
import locale
import uuid


class SkillLevel(Enum):
    """ Player ranking/skill level """
    BEGINNER, STUDENT, GRADUATE, PROFESSIONAL, MASTER, GOD = range(6)


class UserProfile:
    """
    Data about each user.
    May store this in a database later (e.g. SQL)
    """
    def __init__(self, name, birth_date, gender, nationality):
        # personal info
        self.name = name
        self.birth_date = birth_date  # datetime object
        self.gender = gender
        self.nationality = nationality
        
        # https://docs.python.org/3/library/locale.html
        self.language, _ = locale.getlocale()
        
        # game data
        self.wins = 0
        self.games_played = 0
        self.score = 0

    @property
    def user_id(self):
        return self.user_id

    def age(self):
        return (datetime.date.today() - self.birth_date) // 365
    
    def skill_level(self):
        if self.games_played <= 10:
            return SkillLevel.BEGINNER
        else:
            win_percent = self.wins / self.games_played * 100
            if win_percent < 20:
                return SkillLevel.BEGINNER
            elif win_percent < 40:
                return SkillLevel.STUDENT
            elif win_percent < 60:
                return SkillLevel.GRADUATE
            elif win_percent < 80:
                return SkillLevel.PROFESSIONAL
            elif win_percent < 100:
                return SkillLevel.MASTER
            else:
                return SkillLevel.GOD


class Player:
    def __init__(self, profile, user_id, username, password):
        self.cards = []
        self.profile = profile
        self.user_id = uuid.uuid4()
        self.username = username
        self.password = password
        
    @property
    def password(self):
        input("Password: ")
        pass
        
    @password.setter
    def password(self, value):
        pass
