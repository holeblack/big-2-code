from . import game
import datetime

class GameSeries:
    def __init__(self, players, server):
        self.players = players
        self.current_game = None
        self.winner = None
        self.host_server = server  # reference to example-server hosting this seriesself.start_
        self.game_records = []

    @property
    def skill_level(self):
        """ Calculate overall skill level of series.
        Used for check if a new player should join this series.
        """
        total = sum(player.skill_level.value for player in self.players)
        return total / len(self.players)

    def run_series(self):
        """ Game series loop. """
        while True:
            self.current_game = game.Game(self.players, self.players.index(self.winner))
            self.winner = self.current_game.run_game_loop()
            self.game_records.append((datetime.datetime.now(), self.current_game.game_log))
            # remove players who want to leave the series
            player_changed = False
            player_quits = True
            for player in self.players:
                if player_quits:  # not implemented yet (player selects in GUI)
                    self.update_player(player)
                    player_changed = True
            # if players change, then the three of diamonds starts no matter who the winner is
            if player_changed:
                self.winner = None

    def update_player(self, player_to_delete, player_to_add):
        """
        Add list of new players to this series. Should only be called when a full game ends.
        """
        quit_time = datetime.datetime.now()
        self.players.remove(player_to_delete)
        wait_time = datetime.timedelta(seconds=15)
        while datetime.datetime.now() - quit_time <= wait_time:
            for player in self.host_server.queue:
                if player.skill_level == self.skill_level:
                    return player
        # didn't find player within that time
        return self.host_server.queue[0]
