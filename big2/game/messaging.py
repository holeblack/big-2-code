"""
Card representations when sending across networks.

Expected input:
  List of Card objects (see card.py)

Output format:
  JSON-encoded string
  JSON is a list of tuples, where each tuple = (rank_int, suit_int)
  Rank ints: 3, 4, ... A, 2 <--> 0, 1, ..., 11, 12
  Suit ints: Diamond, Club, Heart, Spade <--> 0, 1, 2, 3

Examples:
[Card(Rank.THREE, Suit.DIAMOND)] <--> [(0, 0)]
[Card(Rank.THREE, Suit.CLUB)] <--> [(0, 1)]
[Card(Rank.FOUR, Suit.HEART)] <--> [(1, 2)]
[Card(Rank.TWO, Suit.SPADE), Card(Rank.ACE, Suit.CLUB)] <--> [(12, 3), (11, 1)]
"""

from ..cards.card import Card, Rank, Suit
import json
from typing import List


def cards_to_message(cards: List[Card]) -> str:
    """ Convert list of Card objects to encode message string."""
    return json.dumps([(card.rank.value, card.suit.value) for card in cards])


def message_to_cards(message: str) -> List[Card]:
    """ Decode message string to list of Card objects. """
    return [Card(Rank(r), Suit(s)) for r, s in json.loads(message)]


def simple_message(status, message):
    """ Create a simple JSON string containing a status code and text. """
    return json.dumps(dict(status=status, message=message))
