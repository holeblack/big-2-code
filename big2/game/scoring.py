def calculate_scores(players, winner):
    """
    Calculate
    Args:
    - players: a list of Player objects, each with a 'cards' attribute (list of cards)
    - winner: winning Player object (inside players list)

    Returns:
    - a dictionary with key-value pairs (player object, integer score)
    """
    assert winner in players  # just in case
    losers = [player for player in players if player is not winner]
    scores = dict()
    total_losers_scores = 0
    for loser in losers:
        num_cards = len(loser.cards)
        assert 0 < num_cards <= 13
        if num_cards == 13:
            const = -3
        elif num_cards >= 10:
            const = -2
        else:
            const = -1
        scores[loser] = num_cards * const
        total_losers_scores += scores[loser]

    # Then the winner of the hand scores +1 for every -1 his opponents got
    scores[winner] = -total_losers_scores

    return scores

def consecutive_function():
    # player.stats.win_loss_history = [w, w, w, l, l, w, l, l, w]
    pass


def decay(days):
    """
    If the user stops playing for some amount of days,
    gradually decrease their overall score.
    """
