#!/usr/bin/env python3

"""
Functions that generate possible output combinations for a given set of cards.

These produce generators instead of lists for better efficiency.
(note: use cycle for unlimited suggestions)
"""

from collections import defaultdict
from itertools import combinations, product


def _group_by(values, key):
    """ Group a list of lists according to a given key-function. """
    table = defaultdict(list)
    for value in values:
        table[key(value)] += value
    return table


def _get_n_from_each_group(cards, n, key):
    """ Get n number of cards """
    # could have used chain.from_iterable instead of a loop,
    # but this is more readable IMO
    for rank, group in _group_by(cards, key):
        for combo in combinations(group, n):
            yield combo


def _combine_gen(*gen_list):
    """ Take the Cartesian product of a list of generators. """
    for tup_list in product(*gen_list):
        res = ()
        for tup in tup_list:
            res += tup
        yield res


def single(cards):
    return map(tuple, cards)


def pair(cards):
    return _get_n_from_each_group(cards, 2, lambda c: c.rank)


def triple(cards):
    return _get_n_from_each_group(cards, 3, lambda c: c.rank)


def quadruple(cards):
    return _get_n_from_each_group(cards, 4, lambda c: c.rank)


def straight(cards):
    # note: may also include straight flush
    cards_by_rank = [[] for _ in range(13)]
    for card in cards:
        cards_by_rank[card.rank.value].append(card)
    for i in range(len(cards) - 5):
        group_of_5 = cards_by_rank[i:i+5]  # get each consecutive group of 5
        if all(len(rank_cards) > 0 for rank_cards in group_of_5):
            yield product(*group_of_5)


def flush(cards):
    # note: may also include straight flush
    return _get_n_from_each_group(cards, 5, lambda c: c.suit)


def full_house(cards):
    return _combine_gen(triple(cards), pair(cards))


def four_of_a_kind(cards):
    return _combine_gen(quadruple(cards), single(cards))
    

def straight_flush(cards):
    for st, fl in product(straight(cards), flush(cards)):
        if sorted(st) == sorted(fl):
            yield st
