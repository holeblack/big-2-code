"""
Stuff for representing and determining card combinations.

The functions assume that all cards are unique.
"""
import enum
import collections


def _all_same_rank(cards):
    """ Check if given cards all have same rank. """
    return len(cards) > 0 and all(c.rank == cards[0].rank for c in cards)


def _all_same_suit(cards):
    """ Check if given cards all have same suit. """
    return len(cards) > 0 and all(c.suit == cards[0].suit for c in cards)


def _consecutive_rank(cards):
    """ Check if the cards have consecutive ranks (helper for straight).

    Don't allow loops (e.g. A2345 is not consecutive.)
    """
    if len(cards) == 0:
        return False
    cs = sorted(cards)
    return all(
        curr.rank.value + 1 == next.rank.value
        for curr, next in zip(cs, cs[1:]))


def _count_unique_ranks(cards):
    """ Return the number of cards with each unique rank. """
    return tuple(collections.Counter(c.rank for c in cards).values())


def single(cards):
    """ One card """
    return len(cards) == 1


def pair(cards):
    """ Two cards same rank """
    return len(cards) == 2 and _all_same_rank(cards)


def triple(cards):
    """ Three cards same rank """
    return len(cards) == 3 and _all_same_rank(cards)


def quadruple(cards):
    """ Four cards same rank """
    return len(cards) == 4 and _all_same_rank(cards)


def straight(cards):
    """ Five consecutive rank cards. (for now, don't allow loop)

    Note: can't be a straight flush too.
    """
    return len(cards) == 5 and \
        _consecutive_rank(cards) and not _all_same_suit(cards)


def flush(cards):
    """All five cards have the same suit.

    Note: can't be straight flush too.
    """
    return len(cards) == 5 and \
        _all_same_suit(cards) and not _consecutive_rank(cards)


def full_house(cards):
    """ A triple and a pair. """
    return len(cards) == 5 and \
        _count_unique_ranks(cards) in [(3, 2), (2, 3)]


def four_of_a_kind(cards):
    """A quadruple and a single"""
    return len(cards) == 5 and \
        _count_unique_ranks(cards) in [(4, 1), (1, 4)]


def straight_flush(cards):
    """ Straight and flush. """
    return len(cards) == 5 and \
        _consecutive_rank(cards) and \
        _all_same_suit(cards)


class Combo(enum.Enum):
    """ Constants representing the combination types.  """
    SINGLE, PAIR, TRIPLE, QUADRUPLE,\
    STRAIGHT, FLUSH, FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH = range(9)


_checkers = [
    single,
    pair,
    triple,
    quadruple,
    straight,
    flush,
    full_house,
    four_of_a_kind,
    straight_flush
]
_combo_table = list(zip(Combo, _checkers))


def get_combo_type(cards):
    """ Determine the type of card combination.

    Returns None if cards dont' match a particular combination type.
    """
    for combo_type, is_type in _combo_table:
        if is_type(cards):
            return combo_type
