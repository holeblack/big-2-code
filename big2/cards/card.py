#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Various classes and functions related to Big 2 cards and combinations.

"""
import enum
from collections import namedtuple


# String representations of ranks and suits
_RANK_STR = ["3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A", "2"]
_SUIT_STR = ["♦", "♣", "♥", "♠"]


class Suit(enum.IntEnum):
    DIAMOND, CLUB, HEART, SPADE = range(4)


class Rank(enum.IntEnum):
    THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, \
        JACK, QUEEN, KING, ACE, TWO = range(13)


# Replaces Card class with the simpler 'namedtuple'
Card = namedtuple("Card", ["rank", "suit"])


def pprint(card: Card):
    """ Convenience function for printing out a card. """
    return _RANK_STR[card.rank.value] + _SUIT_STR[card.suit.value]