class BDict:
    """ Bi-directional lookup dictionary.

    Similar to Python's built-in dictionary except
    we can look up a value by key *and* looking up key(s) by value.

    In a BDict, each key maps to a single value
    (NOTE: different keys can map to the same value, many-to-one relationship)

    In its *inverse*, each value maps to a *set* of unique keys.

    For example:

    original Python dict = {'alice': 'room0', 'bob': 'room1', 'carol': 'room1'}

    BDict regular mapping: {'alice': 'room0', 'bob': 'room1', 'carol': 'room1'}

    BDict inverse mapping: {'room0': {'alice'}, 'room1': {'bob', 'carol'}}


    (logic exam practice lol)

    Given a BDict D and its inverse D', the invariant must always hold:

    ∀k,v (D[k] = v ↔ k ϵ D'[v])
    """

    def __init__(self, normal_dict=None, labels=('Key', 'Value')):
        self.labels = labels
        key_val = dict() if normal_dict is None else normal_dict
        self.key_val = dict()  # maps keys to values
        self.val_keys = dict()  # maps values to keys
        for key, val in key_val.items():
            self.set(key, val)

    def set(self, key, value):
        """ Add key-value pair, or update mapping if key already exists. """

        # delete the old key-value mapping if it existed
        if self.has_key(key):
            self.remove_key(key)

        # update inverse (value -> key) direction
        if not self.has_value(value):
            self.val_keys[value] = {key}
        else:
            self.val_keys[value].add(key)

        # update key -> value lookup direction
        self.key_val[key] = value

    def remove_key(self, key):
        """ Remove a key from the dictionary. """
        if not self.has_key(key):
            raise KeyError("'{}' is not a key.".format(key))

        # Remove key from value-keys mapping:
        # 1. if D[k] = v, then k ϵ D'[v], so remove k from D'[v]
        value = self.key_val[key]
        self.val_keys[value].remove(key)

        # 2. if D'[v] = {} then ∄k: D[k] = v, there's no point in keeping D'[v]
        if not self.val_keys[value]:
            del self.val_keys[value]

        # Remove key-value mapping
        del self.key_val[key]

    def remove_value(self, value):
        """ Remove a value from the dictionary. """
        if not self.has_value(value):
            raise ValueError("'{}' is not a value.".format(value))

        # ∀k ϵ D'[v], delete k -> v mapping
        # wrap with list() to avoid "set size changed during iteration" error
        for key in self.get_keys(value).copy():
            self.remove_key(key)

    def get_keys(self, value):
        """ D'[v]. """
        if not self.has_value(value):
            raise ValueError("'{}' is not a value.".format(value))

        return self.val_keys[value]

    def get_value(self, key):
        """ D[k]. """
        if not self.has_key(key):
            raise ValueError("'{}' is not a key.".format(key))
        return self.key_val[key]

    def has_key(self, key):
        """ Check if the given key is in the dictionary. """
        return key in self.key_val

    def has_value(self, value):
        """ Check if the given value is in the dictionary. """
        return value in self.val_keys

    def keys(self):
        """ generate all keys. """
        return self.key_val.keys()

    def values(self):
        """ Generate all values. """
        return self.val_keys.keys()

    def items(self):
        """ Generate all key-value pairs. """
        return self.key_val.items()

    def clear(self):
        """ Delete all data. """
        self.key_val.clear()
        self.val_keys.clear()

    def __str__(self):
        return "BDict:\n\t{k} > {v}: {kv}\n\t{v} > {k}: {vk}".format(
            kv=self.key_val, vk=self.val_keys, k=self.labels[0],
            v=self.labels[1])
