#Bi-Directional Lookup Dictionary
##Purpose
This may be used in the server code. For example, we may want to look up
the room a given client is in, as well as the clients in a given room.
