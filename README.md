# Big 2

Online Multiplayer Big 2 (Cantonese: Cho Dai Di) card game.
Consists of a HTML5 cross-platform mobile app frontend and
a Python3 Tornado server backend.

### Requirements

The server requires Python 3 to run. It is highly recommended to use 
[pip3](https://docs.python.org/3/installing/index.html)
and [virtualenv](https://docs.python.org/3/tutorial/venv.html) to install the required Python packages.
Assuming you have a virtualenv set up (specify Python 3!) and running,
navigate to the root folder and run the following:
```
pip3 install -r requirements.txt
```

## Authors

* **Aaron Fung** - *Programmer*
* **Victor Khong** - *Programmer*