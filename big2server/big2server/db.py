import datetime
import aiopg.sa
from sqlalchemy import (
    MetaData, Table, Column, ForeignKey,
    Integer, String, Enum, DateTime
)

__all__ = ['users']

class SkillLevel(Enum):
    """ Player ranking/skill level """
    BEGINNER, STUDENT, GRADUATE, PROFESSIONAL, MASTER, GOD = range(6)


meta = MetaData()

users = Table(
    'users', meta,
    Column('id', Integer, primary_key=True),
    Column('username', String(20), nullable=False, unique=True),
    Column('password', String(32), nullable=False),
    Column('games_won', Integer, default=0),
    Column('games_played', Integer, default=0),
    Column('skill_level', Integer),
    Column('date_joined', DateTime, default=datetime.datetime.utcnow)
)


async def init_pg(app):
    conf = app['config']['postgres']
    engine = await aiopg.sa.create_engine(
        database=conf['database'],
        user=conf['user'],
        password=conf['password'],
        host=conf['host'],
        port=conf['port'],
        minsize=conf['minsize'],
        maxsize=conf['maxsize'],
    )
    app['db'] = engine


async def close_pg(app):
    app['db'].close()
    await app['db'].wait_closed()
