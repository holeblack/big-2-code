import sys
import logging
import aiohttp
from aiohttp import web

from db import close_pg, init_pg
from settings import get_config

from big2.game import messaging

# Useful debug messages for development
logging.basicConfig(level=logging.DEBUG)
client_logger = logging.getLogger('client-handler')
server_logger = logging.getLogger('server')
room_logger = logging.getLogger('room')


async def init_app(argv=None):
    app = web.Application()
    app['config'] = get_config(argv)
    app['websockets'] = {}
    app.on_startup.append(init_pg)
    app.on_cleanup.append(close_pg)
    app.on_shutdown.append(shutdown)
    # todo: handler
    # app.router.add_get('/', index)
    return app


async def shutdown(app):
    for ws in app['websockets'].values():
        await ws.close()
    app['websockets'].clear()


async def websocket_handler(request):
    ws = web.WebSocketResponse()
    ws_ready = ws.can_prepare(request)
    if not ws_ready.ok:
        pass

    await ws.prepare(request)

    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            if msg.data == 'close':
                await ws.close()
            else:
                ###
                ### main part
                ###
                response = 'something'
                await ws.send_str(response)
        elif msg.type == aiohttp.WSMsgType.ERROR:
            print('ws connection closed with exception %s' %
                  ws.exception())

    print('websocket connection closed')
    return ws


def main(argv):
    logging.basicConfig(level=logging.DEBUG)

    app = init_app(argv)
    config = get_config(argv)
    web.run_app(app)


if __name__ == '__main__':
    main(sys.argv[1:])
