from unittest import TestCase
from bdict.bdict import BDict


class TestBDict(TestCase):

    def test_set(self):
        bdict = BDict()
        bdict.set('a', 1)
        self.assertEqual(bdict.items(), {('a', 1)})
        bdict.set('b', 1)
        self.assertEqual(bdict.items(), {('a', 1), ('b', 1)})
        bdict.set('b', 2)
        self.assertEqual(bdict.items(), {('a', 1), ('b', 2)})

    def test_remove_key(self):
        bdict = BDict({
            'a': 1,
            'b': 1,
            'c': 3
        })
        bdict.remove_key('a')
        self.assertNotIn('a', bdict.keys())
        self.assertIn(1, bdict.values())
        self.assertRaises(KeyError, bdict.remove_key, 'z')
        bdict.remove_key('c')
        self.assertNotIn('c', bdict.keys())
        self.assertNotIn(3, bdict.values())
        self.assertEqual(bdict.items(), {('b', 1)})

    def test_remove_value(self):
        bdict = BDict({
            'a': 1,
            'b': 1,
            'c': 3
        })
        bdict.remove_value(1)
        self.assertNotIn(1, bdict.values())
        bdict = BDict({
            'a': 1,
            'b': 1,
            'c': 3
        })
        bdict.remove_value(3)
        self.assertNotIn(3, bdict.values())
        self.assertRaises(ValueError, bdict.remove_value, 3)


    def test_get_keys(self):
        bdict = BDict({
            'a': 1,
            'b': 1,
            'c': 3
        })
        self.assertSetEqual(bdict.get_keys(1), {'a', 'b'})
        self.assertSetEqual(bdict.get_keys(3), {'c'})
        self.assertRaises(ValueError, bdict.get_keys, 2)

    def test_get_value(self):
        bdict = BDict({
            'a': 1
        })
        self.assertEqual(bdict.get_value('a'), 1)
        self.assertRaises(ValueError, bdict.get_value, 'b')

    def test_has_key(self):
        bdict = BDict({
            'a': 1,
            'z': 2
        })
        self.assertTrue(bdict.has_key('a'))
        self.assertTrue(bdict.has_key('z'))
        self.assertFalse(bdict.has_key('b'))
        self.assertFalse(bdict.has_key(1))

    def test_has_value(self):
        bdict = BDict({
            'a': 1,
            'b': 1,
            'c': 3
        })
        self.assertTrue(bdict.has_value(1))
        self.assertTrue(bdict.has_value(3))
        self.assertFalse(bdict.has_value('a'))
        self.assertFalse(bdict.has_value(2))

    def test_keys(self):
        bdict = BDict({
            'a': 1,
            'b': 2,
            'c': 3,
            'd': 3
        })
        self.assertEqual(bdict.keys(), {'a', 'b', 'c', 'd'})
        bdict = BDict()
        self.assertFalse(bdict.keys())

    def test_values(self):
        bdict = BDict({
            'a': 1,
            'b': 2,
            'c': 3,
            'd': 3
        })
        self.assertEqual(bdict.values(), {1, 2, 3})
        bdict = BDict()
        self.assertFalse(bdict.values())

    def test_items(self):
        bdict = BDict({
            'a': 1,
            'b': 2,
            'c': 3,
            'd': 3
        })
        self.assertEqual(
            bdict.items(), {('a', 1), ('b', 2), ('c', 3), ('d', 3)})
        ks, vs = zip(*bdict.items())
        self.assertCountEqual(ks, bdict.keys())
        self.assertCountEqual(set(vs), bdict.values())
        bdict = BDict()
        self.assertFalse(list(bdict.items()))

    def test_clear(self):
        bdict = BDict()
        bdict.clear()
        self.assertFalse(bdict.keys())
        self.assertFalse(bdict.values())

        bdict = BDict({
            'a': 1,
            'b': 2
        })
        bdict.clear()
        self.assertFalse(bdict.keys())
        self.assertFalse(bdict.values())

    def test_example(self):
        """
            Example use case in the Big 2 example-server - mapping Clients to Rooms.

            With the built-in Python dict, we could lookup which room a client
            is in, e.g. client_room_dict['bob'], but we may also need to lookup
            in the reverse direction, i.e. find out which clients are in a
            particular Room.
            """
        client_room_dict = {
            "alice": "room_0",
            "bob": "room_1",
            "carol": "room_1",
            "dan": "room_1",
            "eve": "room_0",
            "fred": "room_2",
            "george": "room_2"
        }
        bdict = BDict(client_room_dict, ("Client", "Room ID"))
        self.assertSetEqual(bdict.get_keys("room_0"), {"alice", "eve"})
        self.assertSetEqual(bdict.get_keys("room_1"), {"bob", "carol", "dan"})
        self.assertSetEqual(bdict.get_keys("room_2"), {"fred", "george"})

        self.assertEqual(bdict.get_value("alice"), "room_0")
        self.assertEqual(bdict.get_value("eve"), "room_0")
        self.assertEqual(bdict.get_value("dan"), "room_1")
        self.assertEqual(bdict.get_value("fred"), "room_2")

        bdict.remove_key("carol")
        self.assertSetEqual(bdict.get_keys("room_1"), {"bob", "dan"})
        bdict.remove_key("bob")
        self.assertSetEqual(bdict.get_keys("room_1"), {"dan"})
        bdict.remove_key("dan")
        self.assertNotIn("room_1", bdict.values())

        bdict.remove_value("room_2")

        self.assertNotIn("fred", bdict.keys())
        self.assertNotIn("george", bdict.keys())
        bdict.remove_value("room_0")
        self.assertNotIn("alice", bdict.keys())
        self.assertNotIn("eve", bdict.keys())
        self.assertFalse(bdict.keys())
        self.assertFalse(bdict.values())

        bdict = BDict()
        for i in range(10):
            bdict.set(i, "hi")
        self.assertSetEqual(bdict.get_keys("hi"), set(range(10)))

        bdict.remove_value("hi")
        self.assertNotIn("hi", bdict.values())
        self.assertFalse(bdict.keys())
