from unittest import TestCase
from big2.cards.card import Card, Rank, Suit
from big2.cards import combo_check


class TestComboChecker(TestCase):
    def setUp(self):
        self.ca = Card(Rank.ACE, Suit.CLUB)
        self.c2 = Card(Rank.TWO, Suit.CLUB)
        self.c3 = Card(Rank.THREE, Suit.CLUB)
        self.c4 = Card(Rank.FOUR, Suit.CLUB)
        self.c5 = Card(Rank.FIVE, Suit.CLUB)
        self.c6 = Card(Rank.SIX, Suit.CLUB)
        self.c7 = Card(Rank.SEVEN, Suit.CLUB)
        self.c8 = Card(Rank.EIGHT, Suit.CLUB)
        self.d3 = Card(Rank.THREE, Suit.DIAMOND)
        self.d4 = Card(Rank.FOUR, Suit.DIAMOND)
        self.d5 = Card(Rank.FIVE, Suit.DIAMOND)
        self.d9 = Card(Rank.NINE, Suit.DIAMOND)
        self.d10 = Card(Rank.TEN, Suit.DIAMOND)
        self.dj = Card(Rank.JACK, Suit.DIAMOND)
        self.h3 = Card(Rank.THREE, Suit.HEART)
        self.h4 = Card(Rank.FOUR, Suit.HEART)
        self.s3 = Card(Rank.THREE, Suit.SPADE)
        self.s4 = Card(Rank.FOUR, Suit.SPADE)
        self.s7 = Card(Rank.SEVEN, Suit.SPADE)

    def test__all_same_rank(self):
        f = combo_check._all_same_rank
        self.assertFalse(f([]))
        self.assertTrue(f([self.d3]))
        self.assertTrue(f([self.d3, self.h3]))
        self.assertFalse(f([self.d3, self.d4]))
        self.assertTrue(f([self.d3, self.h3, self.c3, self.s3]))
        self.assertFalse(f([self.c3, self.c4, self.c5, self.c6]))

    def test__all_same_suit(self):
        f = combo_check._all_same_suit
        self.assertFalse(f([]))
        self.assertTrue(f([self.d3]))
        self.assertTrue(f([self.d3, self.d4]))
        self.assertTrue(f([self.c3, self.c4, self.c5, self.c6]))
        self.assertFalse(f([self.d3, self.h3, self.c3, self.s3]))

    def test__consecutive_rank(self):
        f = combo_check._consecutive_rank
        self.assertFalse(f([]))
        self.assertTrue(f([self.d3]))
        self.assertTrue(f([self.d3, self.d4, self.d5]))
        self.assertTrue(f([self.d5, self.d4, self.d3]))
        self.assertTrue(f([self.d9, self.d10, self.dj]))
        self.assertTrue(f([self.d10, self.d9, self.dj]))
        self.assertFalse(f([self.ca, self.c2, self.c3]))

    def test__count_unique_ranks(self):
        f = combo_check._count_unique_ranks
        self.assertEqual(f([]), ())
        self.assertEqual(f([self.d3]), (1,))
        self.assertEqual(f([self.c2, self.c3, self.c4]), (1, 1, 1))
        self.assertTrue(f([self.c4, self.h3, self.c4]) in [(1, 2), (2, 1)])

    def test_single(self):
        self.assertTrue(combo_check.single([self.d3]))
        self.assertFalse(combo_check.single([]))
        self.assertFalse(combo_check.single([self.d3, self.d5]))

    def test_pair(self):
        f = combo_check.pair
        self.assertTrue(f([self.s3, self.d3]))
        self.assertTrue(f([self.d4, self.h4]))
        self.assertFalse(f([self.d4, self.d3]))
        self.assertFalse(f([]))
        self.assertFalse(f([self.c3, self.d3, self.h3]))

    def test_triple(self):
        f = combo_check.triple
        self.assertFalse(f([]))
        self.assertFalse(f([self.h3]))
        self.assertFalse(f([self.d3, self.h3]))
        self.assertTrue(f([self.d3, self.c3, self.h3]))
        self.assertFalse(f([self.d3, self.c3, self.h4]))
        self.assertFalse(f([self.s3, self.s7, self.s4]))

    def test_quadruple(self):
        self.assertTrue(combo_check.quadruple(
            [self.d3, self.h3, self.s3, self.c3]))
        self.assertFalse(combo_check.quadruple(
            [self.d3, self.h4, self.s3, self.c3]))

    def test_straight(self):
        f = combo_check.straight
        self.assertTrue(f([self.h4, self.s7, self.c8, self.c5, self.c6]))
        self.assertFalse(f([self.c4, self.c3, self.c7, self.c6, self.c5]))

    def test_flush(self):
        self.assertFalse(combo_check.flush(
            [self.c3, self.c4, self.c5, self.c6, self.c7]))

    def test_full_house(self):
        self.assertTrue(combo_check.full_house(
            [self.s3, self.h4, self.s4, self.c4, self.h3]))

    def test_four_of_a_kind(self):
        self.assertTrue(combo_check.four_of_a_kind(
            [self.c4, self.d4, self.h4, self.d9, self.s4]))

    def test_straight_flush(self):
        f = combo_check.straight_flush
        self.assertTrue(f([self.c3, self.c4, self.c5, self.c6, self.c7]))
        self.assertFalse(f([self.h4, self.s7, self.c8, self.c5, self.c6]))
        self.assertFalse(f([self.c4, self.c7, self.c8, self.c5, self.c3]))

    def test_get_combo_type(self):
        f = combo_check.get_combo_type
        c = combo_check.Combo
        self.assertEqual(
            f([self.c3, self.c4, self.c5, self.c6, self.c7]), c.STRAIGHT_FLUSH)
        self.assertNotEqual(
            f([self.c3, self.c4, self.c5, self.c6, self.c7]), c.STRAIGHT)
        self.assertNotEqual(
            f([self.c3, self.c4, self.c5, self.c6, self.c7]), c.FLUSH)
        self.assertIsNone(f([]))
        self.assertIsNone(f([self.c2, self.c8]))
        self.assertEqual(f([self.c3]), c.SINGLE)
        self.assertEqual(f([self.c3, self.d3]), c.PAIR)
        self.assertNotEqual(f([self.c3, self.c4]), c.PAIR)
        self.assertEqual(f([self.c3, self.d3, self.s3]), c.TRIPLE)
        self.assertNotEqual(f([self.c3, self.d4, self.s3]), c.TRIPLE)
        self.assertNotEqual(f([self.c3, self.c4, self.c5]), c.TRIPLE)
        self.assertEqual(f([self.d4, self.c4, self.h4, self.s4]), c.QUADRUPLE)
        self.assertNotEqual(
            f([self.d3, self.c4, self.h3, self.s4]), c.QUADRUPLE)
