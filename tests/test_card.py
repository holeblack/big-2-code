from unittest import TestCase
from big2.cards.card import Card, Rank, Suit


class TestCard(TestCase):
    def setUp(self):
        self.d3 = Card(Rank.THREE, Suit.DIAMOND)
        self.d4 = Card(Rank.FOUR, Suit.DIAMOND)
        self.d4_copy = Card(Rank.FOUR, Suit.DIAMOND)
        self.h4 = Card(Rank.FOUR, Suit.HEART)

    def test_card_equals(self):
        self.assertTrue(self.d3 == self.d3)
        self.assertTrue(self.d4 == self.d4_copy)

    def test_card_greater(self):
        self.assertTrue(self.d4 > self.d3)

    def test_card_less(self):
        self.assertTrue(self.d3 < self.d4)
        self.assertTrue(self.d4 < self.h4)
